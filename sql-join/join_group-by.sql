/*
You're in a basic SQL sandbox with the following schema.
You can also use commands like `show tables` and `desc messages`

create table messages
(
  id            SERIAL,
  to_email            varchar(100),
  from_email          varchar(100),
  created_at    timestamp without time zone NOT NULL,

  primary key(id)
);

create table smart_folders
(
  id              SERIAL,
  email_to_match varchar(255),

  primary key(id)
);

create table attachments
(
  id            SERIAL,
  message_id    integer not NULL,

  primary key (id)
);


+---------------+---------+           +---------------+---------+
*/

/*
  1.  Write a query that shows the number of messages in each smart folder.
*/


/* Example solution */

select f.id, count(distinct m.id)
from
  smart_folders f
  inner join messages m on f.email_to_match=m.to_email or f.email_to_match=m.from_email
group by f.id;














/*
  2. Write a query that counts the number of attachments per smart folder.
*/

/*
  3. Ensure this query  includes smart folders which have no messages.
*/
