"""
   Node
     V
   L .  R

    Node
     10
   5 .  15

    insert 7
     10
   5 .  15
    7

    insert 13
     10
   5 .  15
    7  13

tree.insert(9)
insert(tree, 9)
"""
# Write a binary search tree with insert operation.
# e.g. in coderpad.io

# Example solution
class Tree:
    def __init__(self, v):
        self.v = v
        self.l = None
        self.r = None

    def insert(self, new_val):
        if new_val < self.v:
            if self.l is None:
                self.l = Tree(new_val)
            else:
                self.l.insert(new_val)
        else:
            if self.r is None:
                self.r = Tree(new_val)
            else:
                self.r.insert(new_val)

    def __str__(self):
        return '{}, l: {}, r: {}'.format(self.v, self.l, self.r)


t = Tree(10)
t.insert(5)
t.insert(15)
t.insert(7)
t.insert(13)
print(t)